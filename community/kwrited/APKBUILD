# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwrited
pkgver=5.23.2
pkgrel=0
pkgdesc="KDE daemon listening for wall and write messages"
arch="all !armhf" # qt5-qtdeclarative-dev  unavilable on armhf
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	knotifications-dev
	kpty-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwrited-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4bd7d378a7ef14ed9b8458d87ee9f0b5fac100f64dbf63e362e29a974e31eddc6783943c82c3876f0104ce922f98504fa91037d04cf781713b8e6af2992fadf4  kwrited-5.23.2.tar.xz
"
