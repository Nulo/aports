# Maintainer: Alex McGrath <amk@amk.ie>
pkgname=fcft
pkgver=2.4.6
pkgrel=0
pkgdesc="A simple library for font loading and glyph rasterization using FontConfig, FreeType and pixman."
url="https://codeberg.org/dnkl/fcft.git"
arch="all"
license="MIT"
makedepends="meson scdoc fontconfig-dev freetype-dev pixman-dev harfbuzz-dev tllist-dev"
checkdepends="check-dev ttf-dejavu" # tests require a font to be installed
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://codeberg.org/dnkl/fcft/archive/$pkgver.tar.gz"
builddir="$srcdir/fcft"

build() {
	abuild-meson . output
	meson compile -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
554d4e01a24aba36eb44c195eef8b82aac08e8e32ddae3dabc45bef20428d02aee981610edecfb9f8311250b5c2c783664b0929305a857dd2910431512a91d95  fcft-2.4.6.tar.gz
"
